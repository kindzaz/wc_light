#define PIN_FET 10
#define PIN_RESISTOR 8
#define PIN_TRIG 14

const short STATUS_ON = 0;
const short STATUS_OFF = 9;
const long BLINK_TIME_INTERVAL = 3UL * 60UL * 1000UL;
const long TURNOFF_AFTER = 9UL * 60UL * 1000UL;

unsigned long startMillis = 0;
long blinkCount = 0;
long dimTimeInterval = 0;
short status = STATUS_ON;

void setup() {
    Serial.begin(9600);

    pinMode(PIN_FET, OUTPUT);
    pinMode(PIN_RESISTOR, INPUT);
    pinMode(PIN_TRIG, INPUT);
    
    dimTimeInterval = map(analogRead(PIN_RESISTOR), 0, 1024, 0, 20) * 1000;

    dim(0, 255, dimTimeInterval);
    startMillis = millis();
    // Serial.print(ledValue);
    // Serial.print("1.ledValue=" + (String) ledValue + "\r\n");
    // blink(10);
    // dim(255, 0, dimTimeInterval);
}

void loop() {
    delay(100);
    if (status == STATUS_OFF) {
        return;
    }

    // Serial.print("(millis() - startMillis)=" + (String) (millis() - startMillis) + "; (10 * 60 * 1000)=" + TURNOFF_AFTER + "\r\n");
    if ((millis() - startMillis) > TURNOFF_AFTER) {
        dim(255, 10, dimTimeInterval);
        status = STATUS_OFF;
    } else if ((millis() - startMillis) / BLINK_TIME_INTERVAL != blinkCount) {
        blinkCount = (millis() - startMillis) / BLINK_TIME_INTERVAL;
        blink(blinkCount);
    }
}

void blink(int times) {
    Serial.print("In blink(). times=" + (String)times + "\r\n");
    for(int i = 0; i < times; i++) {
        dim(255, 120, 300);
        dim(120, 255, 300);
        delay(400);
    }
}

void dim(int ledValueFrom, int ledValueTo, long dimTimeInterval) {
    long startTime = millis();
    long deltaTime = 0;
    float timeFactor = 0;
    while (deltaTime < dimTimeInterval) {
        deltaTime = millis() - startTime;
        timeFactor = (float) deltaTime / dimTimeInterval;
        led(ledValueFrom + pow(timeFactor, 1.2) * (ledValueTo - ledValueFrom));
    }
}

void led(short value) {
    analogWrite(PIN_FET, value);
}



